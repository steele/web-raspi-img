---
layout: page
title: What is an image? Why do I want one anyway?
date: 2020-04-21 12:10:25 -0500
permalink: /what-is-image/
---
Traditionally, all Linux installs were carried out in *regular*
computers (laptops, desktops, servers, etc.) were done by each
distribution's installation programs (for us, that's [Debian
Installer](https://www.debian.org/devel/debian-installer/), also known
as `d-i`). But for platforms such as the Raspberry Pi, some of us
don't think it's the right tool. Among some reasons:

- This class of computers is usually used without access to our
  expected typical interfaces (keyboard and screen), or is done in a
  no-network scenario.
- The typical boot media (a SD or micro-SD card) is slower and less
  reliable than magnetic or solid-state hard drives. Performing a
  regular install to get a base system ready is... a lot of wait, and
  not particularly fun!
- Raspberry Pi computers require a non-free firmware to boot. While it
  is possible to have it built into a `d-i` image and into the
  destination image, it's nontrivial for newer users.
- The Raspberry Pi traditional usage has been always from preinstalled
  images. There is less pushback if we do the same!

Of course, not everything is perfect. In case you download the images
from here, you will be trusting images prepared by just [one
guy](https://gwolf.org/) (who happens, yes, to be a Debian
Developer). The distributed trust among various teams, and the
verification done independently by thousands of users throughout the
world, you can put in an official Debian release are diminished if you
get your image from here. Much even more so if you download a one of
the [daily auto-built images]({{ 'daily-images' | relative_url }}) and
not one of the [tested images]({{ 'tested-images' | relative_url }})!
