# coding: utf-8
# daily_links
module Jekyll
    DailyDir = '/daily'
    TestedDir = '/tested'
    class DailyTable < Liquid::Tag

        def initialize(tag_name, path, tokens)
            super
            @path = path
        end

        def daily_links_for(context, fam)
            ret = []
            dest_files = [ ['Compressed image', 'raspi_%d_%s.img.xz'],
                           ['SHA256 sums', 'raspi_%d_%s.img.xz.sha256'],
                         ]
            distros = ['buster', 'bullseye']

            # Pipe parameter through Liquid to make additional replacements possible
            url = Liquid::Template.parse(@path).render context

            # Adds the site source, so that it also works with a custom one
            site_source = context.registers[:site].config['source']
            distros.each do |distro|
              links = []
              dest_files.each do |dest|
                rel_url = File.join(DailyDir, (dest[1] % [fam, distro]))
                localfile = File.join(context.registers[:site].config['destination'], rel_url)
                if File.exists?(localfile)
                  links << '[%s](%s) <span class="filedata">(%s)</span>' % [dest[0], rel_url, Filedata.for(localfile)]
                else
                  links << '%s not available <!-- %s -->' % [dest[0], rel_url]
                end
              end

              ret << links.join('<br/>')
            end
            return ret.join(' | ')
        end

        def render(context)
          ret = []

          # Pipe parameter through Liquid to make additional replacements possible
          url = Liquid::Template.parse(@distro).render context

          ret << '| Raspberry family | Debian 10 Stable (Buster) | Debian 11 Testing (Bullseye) |'
          ret << '|------------------|---------------------------|------------------------------|'
          ret << '| 0 and 1 | ' + daily_links_for(context, 1)
          ret << '|------------------|---------------------------|------------------------------|'
          ret << '| 2 | ' + daily_links_for(context, 2)
          ret << '|------------------|---------------------------|------------------------------|'
          ret << '| 3 | ' + daily_links_for(context, 3)
          ret << '|------------------|---------------------------|------------------------------|'
          ret << '| 4 | ' + daily_links_for(context, 4)
          return ret.join("\n")
        end
    end

    class DebugTable < Liquid::Tag
        def initialize(tag_name, distro, tokens)
            super
            @distro = distro.strip
        end

        def debug_links_for(context, fam, distro)
            ret = []
            dest_files = [ ['Tarball for the base system', 'raspi_%d_%s.tar.gz'],
                           ['YAML spec for `vmdb` build', 'raspi_%d_%s.yaml'],
                           ['Build log', 'raspi_%d_%s.log']
                         ]

            site_source = context.registers[:site].config['source']
            dest_files.each do |dest|
              rel_url = File.join(DailyDir, (dest[1] % [fam, distro] ))
              localfile = File.join(context.registers[:site].config['destination'], rel_url)
              if File.exists?(localfile)
                ret << '[%s](%s) <span class="filedata">(%s)</span>' % [dest[0], rel_url, Filedata.for(localfile)]
              else
                ret << '%s not available <!-- %s -->' % [dest[0], rel_url]
              end
            end

            return ret.join(' | ')
        end

        def render(context)
            ret = []

            # Pipe parameter through Liquid to make additional replacements possible
            url = Liquid::Template.parse(@distro).render context

            ret << '| Raspberry family | Tarball for the base system                           | YAML spec for `vmdb` build                        | Build log                                       |'
            ret << '|------------------|-------------------------------------------------------|---------------------------------------------------|-------------------------------------------------|'
            ret << '| 0 and 1 | ' + debug_links_for(context, 1, @distro)
            ret << '|------------------|-------------------------------------------------------|---------------------------------------------------|-------------------------------------------------|'
            ret << '| 2       | ' + debug_links_for(context, 2, @distro)
            ret << '|------------------|-------------------------------------------------------|---------------------------------------------------|-------------------------------------------------|'
            ret << '| 3       | ' + debug_links_for(context, 3, @distro)
            ret << '|------------------|-------------------------------------------------------|---------------------------------------------------|-------------------------------------------------|'
            ret << '| 4       | ' + debug_links_for(context, 4, @distro)
            ret << '|------------------|-------------------------------------------------------|---------------------------------------------------|-------------------------------------------------|'
            return ret.join("\n")
        end

    end

    class TestedImgLinks < Liquid::Tag
      def initialize(tag_name, basename, tokens)
        super
        @basename = basename.strip
      end

      def render(context)
        return [ ['xz-compressed image', '/tested/%s.img.xz'],
                 ['sha256sum', '/tested/%s.img.xz.sha256'],
                 ['GPG-signed sha256sum', '/tested/%s.img.xz.sha256.asc'],
               ].map do |item|
          dest = item[1] % @basename

          data = (dest =~ /img.xz$/ ?
                    '<span class="filedata">%s</span>' % Filedata.for(File.join(context.registers[:site].config['destination'], dest)) :
                    '' )

          '[%s](%s) %s' % [item[0], dest, data]
        end.join('<br/>')
      end
    end

end


class Filedata
  def self.for(file)
    begin
      s = File.stat(file)
    rescue Errno::ENOENT
      return '<span class="missing-file">Could not find the file! Something went wrong..?</span>'
    end
      time = s.mtime.strftime '%Y-%m-%d %H:%M:%z'
    size = s.size
    return size > 2**30 ? '%.2f GB, %s' % [size.to_f/2**30, time] :
             size > 2**20 ? '%.2f MB, %s' % [size.to_f/2**20, time] :
               size > 1024 ? '%.2f KB, %s' % [size.to_f/1024, time] :
                 '%d b, %s' % [size, time]
  end
end


Liquid::Template.register_tag('daily_table', Jekyll::DailyTable)
Liquid::Template.register_tag('debug_table', Jekyll::DebugTable)
Liquid::Template.register_tag('tested_img_links', Jekyll::TestedImgLinks)
